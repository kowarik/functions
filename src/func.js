const getSum = (str1, str2) => {
  if((typeof str1 || typeof str2) !== 'string')
    return false;

  str1 = str1.split('').map(x=>+x);
  str2 = str2.split('').map(x=>+x);
  for(let str of str1)
    if(!Number.isInteger(str)) return false;
  for(let str of str2)
    if(!Number.isInteger(str)) return false;

  let result = str1.length >= str2.length ? str1.reverse() : str2.reverse();
  let minStr = str1.length >= str2.length ? str2.reverse() : str1.reverse();
  let minStrLength = minStr.length;
  for(let i = 0; i < minStrLength; i++){
    let add = minStr.shift() + result[i];
    if(add < 10)
      result[i] = add;
    else{
      result[i+1] += 1;
      result[i] = add % 10;
    }
  }
  return result.reverse().join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPost = 0, countComm = 0;

  for(let list of listOfPosts){
    if(list.author === authorName) countPost++;
    if(list.hasOwnProperty('comments')){
      for(let com of list.comments)
        if(com.author === authorName) countComm++;
    }
  }
  return `Post:${countPost},comments:${countComm}`;
};

const tickets=(people)=> {
  let vasyaMoney = 0;
  let ticketCost = 25;
  for(let p of people){
    if (Number(p) - ticketCost > vasyaMoney)
      return `NO`;
    vasyaMoney += ticketCost;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
